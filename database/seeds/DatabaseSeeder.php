<?php

use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Model::unguard();

        App\Order::create([
            'customer' => 'Doni',
            'tipe' => 'coklat-kacang',
            'jumlah' => '3',
            'alamat' => 'Jl. Garuda 20 New York'
        ]);

        App\Order::create([
            'customer' => 'Rini',
            'tipe' => 'keju',
            'jumlah' => '2',
            'alamat' => 'Jl. Juara 12 New York'
        ]);


        Model::reguard();
    }
}
